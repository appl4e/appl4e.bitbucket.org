# README #

**### About ###**

This little project is base on google maps javascript api

### Features ###

It has below features

1. Custom Markers with Filtering system below the map
2. Info window on click on the Marker
3. Click on custom Marker will show path to the targeted building.
4. Showing distance and duration in another info window when showing path
